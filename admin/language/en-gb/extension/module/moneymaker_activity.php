<?php
// Heading
$_['heading_title']     = 'Moneymaker: Customers activity';

// Text
$_['text_extension']    = 'Extensions';
$_['text_success']      = 'Success: You have modified Moneymaker: Customers activity module!';
$_['text_edit']         = 'Edit Module';

// Entry
$_['entry_tab_common']  = 'Common';
$_['entry_tab_help']    = 'Help';
$_['entry_name']        = 'Module Name';
$_['entry_title']       = 'Heading Title';
$_['entry_status']      = 'Status';
$_['entry_customer_name']  = 'Show customers names';
$_['entry_customer_city']  = 'Show customers cities';
$_['entry_customer_date']  = 'Show date and time';
$_['entry_speed']          = 'Scroll speed';
$_['entry_help_text']   = '<b>This software is commercial and copyrighted</b><br /><br />Resale of this software, its assignment, transfer to third parties, putting it in public access and other actions that violate the developers copyright <b>are prohibited</b>.<br /><br />The copyright owner of this software is its <a href="https://opencartforum.com/user/3518-rgb/" target="_blank">author and developer RGB</a>.<br />The only way to contact the developer is through mail: moneymaker.template@gmail.com or via private messages on the sites mentioned below.<br /><br /><b>CAUTION!</b> Beware of scammers and dont risk your own stores!<br /><br />The only websites where you can purchase this module at the time of the release of the current version:<br /><br /> &bull; <a href="https://opencartforum.com/" target="_blank">The largest OpenCart community in the CIS</a><br /> &bull; <a href="https://liveopencart.ru/" target="_blank">LiveOpencart marketplace</a><br /> &bull; <a href="http://shop.opencart-russia.ru/" target="_blank">Opencart-russia.ru marketplace</a><br /> &bull; <a href="https://prodelo.biz/" target="_blank">ProDelo.biz marketplace</a><br /><br /><b>If you downloaded the module for free</b> (more precisely, a set of files of unknown origin that are given out for this module), bought the module in another place, on another sites, transferred money to some unknown accounts or wallets - you were deceived, and your store is now potentially vulnerable. You will not be able to receive module updates and you will not be able to ask for the technical support, and the sites where the module will be installed will violate the licensing conditions, the measures indicated below will be taken.<br /><br /><b>ATTENTION!</b> Illegal use of the module, as well as the use of the module without purchasing additional licenses for more than 1 domain, is prohibited, if such precedents are found, you will lose the possibility of updating, you will be denied for the further technical support, and your sites with your accounts will be blacked out a list with further requests to the provider.<br /><br />In addition to being blocked for violating the terms of the license, you and your store will receive a negative review and the corresponding reputation on the most visited OpenCart resource in the CIS, after which no serious developer will work with you, understanding your attitude to other peoples work.<br /><br />By default, the license for the module is valid for 1 customers domain; to use the module on several domains, you must purchase additional licenses for each of them (you will be able to use the system of cumulative discounts).<br /><br />This software is provided "as is". The copyright owner does not provide any guarantees regarding the error-free and uninterrupted operation of the software, its compliance with specific goals and expectations of the user, the safety of files and/or user data.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Moneymaker: Customers activity module!';
$_['error_name']        = 'Module Name must be between 3 and 64 characters!';
