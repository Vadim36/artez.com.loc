<?php
// Heading
$_['heading_title']     = 'Moneymaker: Активність покупців';

// Text
$_['text_extension']    = 'Модулі';
$_['text_success']      = 'Ви успішно встановили/оновили модуль!';
$_['text_edit']         = 'Змінити модуль';

// Entry
$_['entry_tab_common']  = 'Основне';
$_['entry_tab_help']    = 'Допомога';
$_['entry_name']        = 'Назва';
$_['entry_title']       = 'Заголовок';
$_['entry_status']      = 'Статус';
$_['entry_customer_name']  = 'Показувати імена відвідувачів';
$_['entry_customer_city']  = 'Показувати міста відвідувачів';
$_['entry_customer_date']  = 'Показувати дату та час';
$_['entry_speed']          = 'Швидкість прокрутки';
$_['entry_help_text']   = '<b>Це програмне забезпечення є комерційним і захищено авторським правом</b><br /><br />Перепродаж даного програмного забезпечення, його привласнення, передача третім особам, викладання в публічний доступ та інші дії, що порушують авторське право розробника, <b>заборонені</b>.<br /><br />Правовласником даного програмного забезпечення є його <a href="https://opencartforum.com/user/3518-rgb/" target="_blank">автор і розробник RGB</a>.<br />Єдиний спосіб зв"язатися з розробником - це через пошту: moneymaker.template@gmail.com або через особисті повідомлення на веб-сайтах, що згадуються нижче.<br /><br /><b>ОБЕРЕЖНО!</b> Остерігайтеся шахраїв і не піддавайте ризику власні магазини!<br /><br />Єдині веб-сайти, де можна придбати даний модуль на момент виходу поточної версії:<br /><br /> &bull; <a href="https://opencartforum.com/" target="_blank">Найбільша спільнота OpenCart в СНД</a><br /> &bull; <a href="https://liveopencart.ru/" target="_blank">Магазин доповнень LiveOpencart</a><br /> &bull; <a href="http://shop.opencart-russia.ru/" target="_blank">Магазин доповнень Opencart-russia.ru</a><br /> &bull; <a href="https://prodelo.biz/" target="_blank">Магазин доповнень ProDelo.biz</a><br /><br /><b>Якщо ви завантажили модуль безкоштовно</b> (точніше, набір файлів невідомого походження, який видають за даний модуль), купили модуль в іншому місці, на іншому сайті, перевели гроші на якісь невідомі рахунки або гаманці - вас обдурили, а ваш магазин тепер потенційно вразливий. Ви не зможете отримувати оновлення модуля і не зможете розраховувати на технічну підтримку, а сайти, де буде встановлений модуль, будуть порушувати умови ліцензування, до них будуть вжиті заходи, зазначені далі.<br /><br /><b>УВАГА!</b> Нелегальне використання модуля, а також використання модуля без покупки додаткових ліцензій на більш, ніж 1 домені заборонено, в разі виявлення таких прецедентів ви втратите можливість оновлення, вам буде відмовлено в подальшій техпідтримці, а ваші сайти, як і ваші аккаунти, будуть внесені в чорний список з подальшим зверненням до провайдера.<br /><br />Крім блокування за порушення умов ліцензії, ви і ваш магазин отримаєте негативний відгук і відповідну репутацію на найбільш відвідуваному ресурсі по OpenCart в СНД, після чого жоден серйозний розробник не буде з вами працювати, розуміючи ваше ставлення до чужої праці.<br /><br />За замовчуванням ліцензія на модуль поширюється на 1 домен покупця, для використання модуля на кількох доменах ви повинні придбати додаткові ліцензії на кожен з них, при цьому вам буде доступна система накопичувальних знижок.<br /><br />Це програмне забезпечення надається на умовах "як є" (as is). Правовласник не надає ніяких гарантій щодо безпомилкової та безперебійної роботи програмного забезпечення, його відповідності конкретним цілям і очікуванням користувача, збереження файлів і/або даних користувача.';

// Error
$_['error_permission']  = 'У вас немає доступу до зміни модулю!';
$_['error_name']        = 'Назва модулю повинна бути від 3 до 64 символів!';
